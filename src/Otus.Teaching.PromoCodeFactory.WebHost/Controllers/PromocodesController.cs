﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IPromocodeMapper _promocodeMapper;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public PromocodesController(IRepository<PromoCode> promocodeRepository
            , IRepository<Customer> customerRepository
            , IRepository<Preference> preferenceRepository
            , IPromocodeMapper promocodeMapper)
        {
            _promocodeMapper = promocodeMapper;
            _promocodeRepository = promocodeRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();

            var response = promocodes.Select(x => _promocodeMapper.PromocodeToShortResponse(x)).ToList();
            return Ok(response);

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            // Здесь пришлось выдавать промокод только одному клиенту, т.к. согласно п.4 заданияЖ
            // "Связь Customer и Promocode реализовать через One-To-Many, будем считать, что в данном примере промокод может быть выдан только одному клиенту из базы"


            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(x => x.Name == request.Preference);
            if (preference == null)
                return NotFound();
            var customers = await _customerRepository.GetAllAsync();
            var customer = customers.FirstOrDefault(x => x.PromoCodePreferences.Any(x => x.PreferenceId == preference.Id));
            if (customer == null)
                return NotFound();

            var promoCode = _promocodeMapper.PromocodeFromRequest(request, preference, customer, Guid.NewGuid());

            await _promocodeRepository.AddAsync(promoCode);

            return Ok();

        }
    }
}
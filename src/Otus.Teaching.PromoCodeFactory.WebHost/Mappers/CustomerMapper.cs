﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper : ICustomerMapper
    {
        private readonly IPreferenceMapper _preferenceMapper;
        private readonly IPromocodeMapper _promocodeMapper;
        public CustomerMapper(IPreferenceMapper preferenceMapper,
            IPromocodeMapper promocodeMapper)
        {
            _preferenceMapper = preferenceMapper;
            _promocodeMapper = promocodeMapper;
        }

        public CustomerShortResponse CustomerToShortResponse(Customer customer)
        {
            return new CustomerShortResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }

        public CustomerResponse CustomerToResponse(Customer customer)
        {
            return new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.PromoCodePreferences.Select(x => _preferenceMapper.PreferenceToResponse(x.Preference)).ToList(),
                PromoCodes = customer.PromoCodes.Select(x => _promocodeMapper.PromocodeToShortResponse(x)).ToList()
            };
        }

        public Customer RequestToCustomer(CreateOrEditCustomerRequest request, Guid id)
        {
            return new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Id = id,
                PromoCodePreferences = request.PreferenceIds.Select(x => new CustomerPreference() { CustomerId = id, PreferenceId = x }).ToList()
            };
        }

        public Customer RequestToCustomer(CreateOrEditCustomerRequest request, Customer customer)
        {
            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.PromoCodePreferences = request.PreferenceIds.Select(x => new CustomerPreference() { CustomerId = customer.Id, PreferenceId = x }).ToList();
            return customer;

        }
    }
}

﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public interface IPromocodeMapper
    {
        PromoCodeShortResponse PromocodeToShortResponse(PromoCode promocode);
        PromoCode PromocodeFromRequest(GivePromoCodeRequest request, Preference preference, Customer customer, Guid id);
    }
}

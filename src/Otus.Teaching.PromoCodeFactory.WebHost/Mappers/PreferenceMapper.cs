﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PreferenceMapper : IPreferenceMapper
    {
        public PreferenceResponse PreferenceToResponse(Preference preference)
        {
            return new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name
            };
        }

    }
}
